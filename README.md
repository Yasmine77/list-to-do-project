# ListToDoProject

- build a React Todo List App.  
- add, remove, edit, and mark complete/cross out the todo items. 
- Creating the Components
- TodoForm Component
- TodoList Component
- the Todo Component
- Adding the CSS
- Creating Custom Edit Input
- Filtering tasks (all,complete,incomplete)

## Install

- npm i

## screenshot

### ListToDo Screen(All Tasks)

![](Pictures/Screenshot_4.png)

### ListToDo Screen(complete Tasks)

![](Pictures/Screenshot_5.png)

### ListToDo Screen(incomplete Tasks)

![](Pictures/Screenshot_6.png)

### ListToDo Screen(update Tasks)

![](Pictures/Screenshot_update.png)



